package com.somospnt.techiespringvalidation.domain.validation;

import com.somospnt.techiespringvalidation.domain.Empleado;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FechaIngresoMenorFechaEgresoValidator implements ConstraintValidator<FechaIngresoMenorFechaEgreso, Empleado> {

    @Override
    public boolean isValid(Empleado empleado, ConstraintValidatorContext context) {
        return empleado.getFechaEgreso().isAfter(empleado.getFechaIngreso());
    }
}
