package com.somospnt.techiespringvalidation.domain.validation;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Repeatable(MenorQue.List.class)
@Constraint(validatedBy = MenorQueValidator.class)
@Documented
public @interface MenorQue {

    String message() default "{atributoMenor} debe ser menor que {atributoMayor}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String atributoMenor();

    String atributoMayor();

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {

        MenorQue[] value();
    }
}
