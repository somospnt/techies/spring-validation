package com.somospnt.techiespringvalidation.domain.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = FechaIngresoMenorFechaEgresoValidator.class)
public @interface FechaIngresoMenorFechaEgreso {

    String message() default "la fecha ingreso debe ser menor a la fecha de egreso";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
