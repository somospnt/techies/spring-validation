package com.somospnt.techiespringvalidation.domain.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class MenorQueValidator implements ConstraintValidator<MenorQue, Object> {

    private String atributoMenor;
    private String atributoMayor;

    @Override
    public void initialize(final MenorQue annotation) {
        atributoMenor = annotation.atributoMenor();
        atributoMayor = annotation.atributoMayor();
    }

    @Override
    public boolean isValid(final Object objeto, final ConstraintValidatorContext context) {
        BeanWrapper beanWrapper = new BeanWrapperImpl(objeto);
        Comparable valorMenor = (Comparable) beanWrapper.getPropertyValue(atributoMenor);
        Comparable valorMayor = (Comparable) beanWrapper.getPropertyValue(atributoMayor);
        return valorMenor != null && valorMenor.compareTo(valorMayor) < 0;
    }

}
