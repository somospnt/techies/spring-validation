package com.somospnt.techiespringvalidation.domain.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.h2.util.StringUtils;

public class EmpiezaConMayusculaValidator implements ConstraintValidator<EmpiezaConMayuscula, String> {

    @Override
    public boolean isValid(String texto, ConstraintValidatorContext context) {
        if (StringUtils.isNullOrEmpty(texto)) {
            return true;
        } else {
            String primeraLetra = texto.substring(0, 1);
            return primeraLetra.toUpperCase().equals(primeraLetra);
        }
    }
}
