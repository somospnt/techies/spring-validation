package com.somospnt.techiespringvalidation.domain.validation;

import static java.lang.annotation.ElementType.FIELD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = EmpiezaConMayusculaValidator.class)
public @interface EmpiezaConMayuscula {

    String message() default "debe comenzar con mayúscula";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
