package com.somospnt.techiespringvalidation.domain;

import com.somospnt.techiespringvalidation.domain.validation.EmpiezaConMayuscula;
import com.somospnt.techiespringvalidation.domain.validation.MenorQue;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
//@FechaIngresoMenorFechaEgreso
@MenorQue(atributoMenor = "fechaIngreso", atributoMayor = "fechaEgreso")
@MenorQue(atributoMenor = "sueldoNeto", atributoMayor = "sueldoBruto")
public class Empleado {

    @Id
    private Long id;
    @EmpiezaConMayuscula
    @NotBlank
    private String nombre;
    private LocalDate fechaIngreso;
    private LocalDate fechaEgreso;
    private BigDecimal sueldoNeto;
    private BigDecimal sueldoBruto;

}
