package com.somospnt.techiespringvalidation.service;

import com.somospnt.techiespringvalidation.domain.Empleado;
import javax.transaction.Transactional;
import org.h2.util.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EmpleadoServiceSinValidator {

    public void guardar(Empleado empleado) {
        if (StringUtils.isNullOrEmpty(empleado.getNombre()) || empleado.getNombre().isBlank()) {
            throw new IllegalArgumentException("El empleado debe tener nombre");
        }
        if (!primeraLetraMayuscula(empleado.getNombre())) {
            throw new IllegalArgumentException("El nombre del empleado debe comenzar con mayusculas");
        }
    }

    private boolean primeraLetraMayuscula(String texto) {
        String primeraLetra = texto.substring(0, 1);
        return primeraLetra.toUpperCase().equals(primeraLetra);
    }
}
