package com.somospnt.techiespringvalidation.service;

import com.somospnt.techiespringvalidation.domain.Empleado;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Transactional
@Validated
public class EmpleadoServiceConSpringValidator {    
    
    public void guardar(@Valid Empleado empleado) {
    }
}
