package com.somospnt.techiespringvalidation.service;

import com.somospnt.techiespringvalidation.domain.Empleado;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import org.springframework.stereotype.Service;

@Service
public class EmpleadoServiceConBeanValidation {    
    
    public void guardar(Empleado empleado) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Empleado>> errores = validator.validate(empleado);
        if (!errores.isEmpty()) {
            throw new ConstraintViolationException(errores);
        }
    }
}
