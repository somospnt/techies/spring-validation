package com.somospnt.techiespringvalidation.service;

import com.somospnt.techiespringvalidation.ApplicationTests;
import com.somospnt.techiespringvalidation.domain.Empleado;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.validation.ConstraintViolationException;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/*
 * Ejercicio:
 *
 * Constraints de especificación JSR Validation:
 * https://javaee.github.io/javaee-spec/javadocs/javax/validation/constraints/package-summary.html
 * Constraints de Hibernate Validation:
 * https://docs.jboss.org/hibernate/stable/validator/api/
 *
 */
public class EmpleadoServiceTest extends ApplicationTests {

    @Autowired
    private EmpleadoServiceConSpringValidator empleadoService;

    @Test // Ya resuelto
    public void guardar_conNombreNull_lanzaExcepcion() {
        Empleado empleado = empleadoValido();
        empleado.setNombre(null);

        assertThatThrownBy(() -> empleadoService.guardar(empleado))
                .isInstanceOf(ConstraintViolationException.class)
                .hasMessageContaining("nombre");
    }

    @Test
    public void guardar_conNombreVacio_lanzaExcepcion() {
        Empleado empleado = empleadoValido();
        empleado.setNombre("");

        assertThatThrownBy(() -> empleadoService.guardar(empleado))
                .isInstanceOf(ConstraintViolationException.class)
                .hasMessageContaining("nombre");
    }

    @Test
    public void guardar_conNombreSoloEspacios_lanzaExcepcion() {
        Empleado empleado = empleadoValido();
        empleado.setNombre("          ");

        assertThatThrownBy(() -> empleadoService.guardar(empleado))
                .isInstanceOf(ConstraintViolationException.class)
                .hasMessageContaining("nombre");
    }

    @Test
    public void guardar_conNombreEmpiezaConMinuscula_lanzaExcepcion() {
        Empleado empleado = empleadoValido();
        empleado.setNombre("horacio");

        assertThatThrownBy(() -> empleadoService.guardar(empleado))
                .isInstanceOf(ConstraintViolationException.class)
                .hasMessageContaining("nombre");
    }

    @Test
    public void guardar_conFechaEgresoMenorFechaIngreso_lanzaExcepcion() {
        Empleado empleado = empleadoValido();
        empleado.setFechaEgreso(empleado.getFechaIngreso().minusDays(1));
        assertThatThrownBy(() -> empleadoService.guardar(empleado))
                .isInstanceOf(ConstraintViolationException.class)
                .hasMessageContaining("fechaIngreso debe ser menor que fechaEgreso");
    }

    @Test
    public void guardar_conSueldoBrutoMenorSueldoNeto_lanzaExcepcion() {
        Empleado empleado = empleadoValido();
        empleado.setSueldoBruto(empleado.getSueldoNeto().subtract(BigDecimal.TEN));
        assertThatThrownBy(() -> empleadoService.guardar(empleado))
                .isInstanceOf(ConstraintViolationException.class)
                .hasMessageContaining("sueldoNeto debe ser menor que sueldoBruto");
    }

    private Empleado empleadoValido() {
        Empleado empleado = new Empleado();
        empleado.setNombre("Horacio");
        empleado.setFechaIngreso(LocalDate.now().minusMonths(1));
        empleado.setFechaEgreso(LocalDate.now());
        empleado.setSueldoNeto(new BigDecimal("50000"));
        empleado.setSueldoBruto(new BigDecimal("100000"));
        return empleado;
    }

}
